'use strict';

var boxIndex = {
  init: function() { 
    this.dom();
    this.events();
  },
  dom: function() {  
    this.$clicks = $('.click-me')
    this.$target = this.$clicks.eq(2);
  },
  events: function() {
    var target = this.$target;
    var clickTrigger = this.$clicks

    target.css('backgroundColor', 'red')
    // click event
    clickTrigger.click(function() {
      alert("Watch out!! Got You!");
    })
  }
}
boxIndex.init()
